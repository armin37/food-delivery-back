package com.tosan.delivery.services;

import com.github.javafaker.Faker;
import com.tosan.delivery.DB.VendorRepository;
import com.tosan.delivery.models.Product;
import com.tosan.delivery.models.Vendor;

import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;

public class VendorService {
    private static VendorService self;
    private static VendorRepository vendorRepository = VendorRepository.getInstance();

    public static VendorService getInstance() {
        if (self == null)
            self = new VendorService();
        return self;
    }

    public VendorService() {
        Faker faker = new Faker();
        Integer foodId = 1;
        Random r = new Random();
        for (int i = 1; i < 4; i++) {
            String name = faker.company().name();
            Vendor vendor = new Vendor(name.replaceAll(" ", "-"), name, LocalDateTime.now());
            for (int j = 1; j < 5; j++) {
                String foodName = faker.food().dish();
                Product food = new Product(foodId++, i, foodName.replaceAll(" ", "-"), foodName, r.nextInt(50 - 5) + 5, r.nextInt(90 - 10) + 10, LocalDateTime.now());
                vendor.getProducts().add(food);
            }
            vendorRepository.save(vendor);
        }
    }

    public List<Vendor> findAll() {
        return vendorRepository.findAll();
    }

    public Vendor findById(Integer id) {
        return vendorRepository.find(id);
    }

    public Vendor findBySlug(String slug) {
        return vendorRepository.findBySlug(slug);
    }

    public Product findProductInVendor(Vendor vendor, Integer productId) {
        return vendor.getProducts().stream().filter(item -> item.getId() == productId).findFirst().orElse(null);
    }
}
