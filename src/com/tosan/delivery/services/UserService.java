package com.tosan.delivery.services;

import com.tosan.delivery.DB.DB;
import com.tosan.delivery.DB.UserRepository;
import com.tosan.delivery.models.User;

import java.util.List;
import java.util.Scanner;

public class UserService {
    private static UserService self;
    private static UserRepository userRepository = UserRepository.getInstance();

    public static UserService getInstance() {
        if (self == null)
            self = new UserService();
        return self;
    }

    public User registerUser() {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter user phone: ");
        String phone = in.nextLine();
        User user = userRepository.findByPhone(phone);
        if (user != null) {
            System.out.println("Duplicate phone!!");
            return null;
        }

        System.out.print("Enter name: ");
        String name = in.nextLine();

        System.out.print("Enter password: ");
        String password = in.nextLine();

        User saveUser = new User(phone, name, password);
        userRepository.save(saveUser);
        return saveUser;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findById(Integer id) {
        return userRepository.find(id);
    }

    public User findByPhone(String phone) {
        return userRepository.findByPhone(phone);
    }
}
