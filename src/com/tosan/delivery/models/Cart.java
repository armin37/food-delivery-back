package com.tosan.delivery.models;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

public class Cart {
    private static Cart self;
    User user;
    Set<Product> cart = new LinkedHashSet<>();
    Vendor selectedVendor;
    int price;
    int processTime;

    public static Cart getInstance() {
        if (self == null) {
            self = new Cart();
        }
        return self;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Product> getCart() {
        return cart;
    }

    public void setCart(Set<Product> cart) {
        this.cart = cart;
    }

    public Vendor getSelectedVendor() {
        return selectedVendor;
    }

    public void setSelectedVendor(Vendor selectedVendor) {
        this.selectedVendor = selectedVendor;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getProcessTime() {
        return processTime;
    }

    public void setProcessTime(int processTime) {
        this.processTime = processTime;
    }
}
