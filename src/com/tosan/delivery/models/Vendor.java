package com.tosan.delivery.models;

import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

public class Vendor {
    private int id;
    private String name;
    private String slug;
    private LocalDateTime createdAt;
    private Set<Product> products = new LinkedHashSet<>();

    public Vendor(String slug, String name, LocalDateTime createdAt) {
        this.slug = slug;
        this.name = name;
        this.createdAt = createdAt;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Vendor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
