package com.tosan.delivery.models;

import java.time.LocalDateTime;
import java.util.Objects;

public class Product {
    private Integer id;
    private Integer vendorId;
    private String slug;
    private String name;
    private int price;
    private int processTime;
    private LocalDateTime createdAt;

    public Product(Integer id, Integer vendorId, String slug, String name, int price, int processTime, LocalDateTime createdAt) {
        this.id = id;
        this.vendorId = vendorId;
        this.slug = slug;
        this.name = name;
        this.price = price;
        this.processTime = processTime;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVendorId() {
        return vendorId;
    }

    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getProcessTime() {
        return processTime;
    }

    public void setProcessTime(int processTime) {
        this.processTime = processTime;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product food = (Product) o;
        return Objects.equals(id, food.id) && Objects.equals(slug, food.slug);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, slug);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", processTime=" + processTime +
                '}';
    }
}
