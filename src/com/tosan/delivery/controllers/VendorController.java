package com.tosan.delivery.controllers;

import com.tosan.delivery.models.*;
import com.tosan.delivery.services.VendorService;

import java.util.*;

public class VendorController {
    private static VendorController self;
    private static Cart session = Cart.getInstance();
    private static VendorService vendorService = VendorService.getInstance();

    public static VendorController getInstance() {
        if (self == null) {
            self = new VendorController();
        }
        return self;
    }

    public void showAllVendors() {
        List<Vendor> vendors = vendorService.findAll();
        vendors.forEach(System.out::println);
    }

    public void selectVendor() {
        showAllVendors();
        System.out.print("Enter Vendor Id: ");
        Scanner scanner = new Scanner(System.in);
        Integer id = scanner.nextInt();
        Vendor vendor = vendorService.findById(id);
        if (vendor == null) {
            System.out.print("Vendor Not Found: ");
            return;
        }

        vendor.getProducts().forEach(System.out::println);

        while (true) {
            System.out.print("Enter Food Id(Add to cart)(Enter 0 to Finish): ");
            Integer productId = scanner.nextInt();
            switch (productId) {
                case 0:
                    return;
                default:
                    addToCart(vendor, productId);
            }
        }
    }

    private void printVendorFoods(Vendor vendor) {
        vendor.getProducts().forEach(System.out::println);
    }

    private void addToCart(Vendor vendor, Integer productId) {
        if (session.getSelectedVendor() == null) {
            session.setSelectedVendor(vendor);
        }
        if (!session.getSelectedVendor().equals(vendor)) {
            System.out.println("Another Vendor in cart(First Clear Cart)");
            return;
        }
        Product product = vendorService.findProductInVendor(vendor, productId);
        if (product == null) {
            System.out.println("Product Not Found");
            return;
        }
        Product check = session.getCart().stream().filter(item -> item.getId() == productId).findFirst().orElse(null);
        if (check != null) {
            System.out.println("Already In Cart");
            return;
        }
        session.getCart().add(product);
        System.out.println("Successfully added to cart");
    }

    public void printCart() {
        session.getCart().forEach(System.out::println);
    }

    public void clearCart() {
        session.setCart(new LinkedHashSet<>());
        session.setSelectedVendor(null);
    }

    public boolean placeOrder() {
        if (session.getUser() == null) {
            System.out.println("Login First");
            return false;
        }

        if (session.getSelectedVendor() == null || session.getCart().isEmpty()) {
            System.out.println("Cart Is Empty");
            return false;
        }

        Optional<Product> productWithMaxProcessTime = session.getCart().stream().max(Comparator.comparing(Product::getProcessTime));
        if (productWithMaxProcessTime == null) {
            return false;
        }
        session.setProcessTime(productWithMaxProcessTime.get().getProcessTime());
        session.setPrice(session.getCart().stream().map(item -> item.getPrice()).reduce(Integer::sum).get());

        System.out.println(String.format("Dear %s, Your order was placed successfully And will be arrived in %s minutes", session.getUser().getFullName(), session.getProcessTime()));
        System.out.println("Here is your order summary: ");
        session.getCart().forEach((n) -> {
            System.out.printf("%-30s %-30s\n", n.getName(), n.getPrice() + "$");
        });
        System.out.printf("%-30s %-30s\n", "Total Price", session.getPrice() + "$");
        return true;
    }
}
