package com.tosan.delivery.controllers;

import com.tosan.delivery.models.Cart;
import com.tosan.delivery.models.User;
import com.tosan.delivery.services.UserService;

import java.util.List;
import java.util.Scanner;

public class UserController {
    private static UserController self;
    private static UserService userService = UserService.getInstance();
    private static Cart session = Cart.getInstance();

    public static UserController getInstance() {
        if (self == null) {
            self = new UserController();
        }
        return self;
    }

    public void register() {
        userService.registerUser();
    }

    public void showAllUsers() {
        List allUsers = userService.findAll();
        allUsers.forEach(System.out::println);
    }

    public void login() {
        System.out.print("Enter User Phone: ");
        Scanner scanner = new Scanner(System.in);
        String phone = scanner.nextLine();
        User user = userService.findByPhone(phone);
        if (user == null) {
            System.out.println("User Not Found!");
            return;
        }
        System.out.print("Enter Password: ");
        String password = scanner.nextLine();
        if (!password.equals(user.getPassword())) {
            System.out.println("Wrong Password!");
            return;
        }
        System.out.println("Logged In Successfully");
        session.setUser(user);
    }
}
