package com.tosan.delivery;

import com.tosan.delivery.controllers.UserController;
import com.tosan.delivery.controllers.VendorController;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        UserController userController = UserController.getInstance();
        VendorController vendorController = VendorController.getInstance();
        boolean placedOrder = false;

        while (!placedOrder) {
//            Runtime.getRuntime().exec("cls");

            System.out.println();
            System.out.printf("%-30s %-30s %-30s\n", "1.New User", "2.Show All Users", "3.Login");
            System.out.printf("%-30s %-30s %-30s %-30s\n", "4.Select Vendor", "5.Print Cart", "6.Place Order", "7.Clear Cart");
            System.out.print("Enter your selection: ");
            Scanner scanner = new Scanner(System.in);
            int input = scanner.nextInt();

            switch (input) {
                case 1:
                    userController.register();
                    break;

                case 2:
                    userController.showAllUsers();
                    break;

                case 3:
                    userController.login();
                    break;

                case 4:
                    vendorController.selectVendor();
                    break;

                case 5:
                    vendorController.printCart();
                    break;

                case 6:
                    placedOrder = vendorController.placeOrder();
                    break;

                case 7:
                    vendorController.clearCart();
                    break;
            }
        }
    }
}
