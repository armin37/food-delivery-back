package com.tosan.delivery.DB;

import com.tosan.delivery.models.Vendor;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class VendorRepository implements DB<Vendor, Integer> {
    Set<Vendor> vendors = new LinkedHashSet<>();
    private static VendorRepository instance;
    private static Integer vendorIncrement = 1;

    public static VendorRepository getInstance() {
        if (instance == null) {
            instance = new VendorRepository();
        }
        return instance;
    }

    @Override
    public Vendor save(Vendor vendor) {
        vendor.setId(vendorIncrement++);
        vendors.add(vendor);
        return vendor;
    }

    @Override
    public List<Vendor> findAll() {
        return new ArrayList<Vendor>(vendors);
    }

    @Override
    public Vendor find(Integer id) {
        return vendors.stream().filter(item -> item.getId() == id).findFirst().get();
    }

    public Vendor findBySlug(String slug) {
        return vendors.stream().filter(item -> item.getSlug().equals(slug)).findFirst().get();
    }

    public Set<Vendor> getVendors() {
        return vendors;
    }

    public void setVendors(Set<Vendor> vendors) {
        this.vendors = vendors;
    }
}
