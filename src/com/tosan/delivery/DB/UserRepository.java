package com.tosan.delivery.DB;

import com.tosan.delivery.models.User;

import java.util.*;

public class UserRepository implements DB<User, Integer> {
    Set<User> users = new LinkedHashSet<>();
    private static UserRepository instance;
    private static Integer userIncrement = 1;

    public static UserRepository getInstance() {
        if (instance == null) {
            instance = new UserRepository();
        }
        return instance;
    }

    @Override
    public User save(User user) {
        user.setId(userIncrement++);
        users.add(user);
        return user;
    }

    @Override
    public List<User> findAll() {
        return new ArrayList<User>(users);
    }

    @Override
    public User find(Integer id) {
        return users.stream().filter(item -> item.getId() == id).findFirst().get();
    }

    public User findByPhone(String phone) {
        return users.stream().filter(item -> item.getPhone().equals(phone)).findFirst().orElse(null);
    }
}
