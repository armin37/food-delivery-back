package com.tosan.delivery.DB;

import com.tosan.delivery.models.User;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface DB<T, ID> {
    List<T> findAll();

    <S extends T> S find(ID id);

    <S extends T> S save(S entity);
}
